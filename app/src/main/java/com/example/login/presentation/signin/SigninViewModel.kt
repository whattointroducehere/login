package com.example.login.presentation.signin

import android.text.TextUtils
import android.widget.Toast
import com.example.login.R
import com.example.login.domain.usecase.AccountUsecase
import com.example.login.presentation.base.BaseViewModelImpl
import io.reactivex.disposables.Disposable
import java.net.UnknownHostException

class SigninViewModel(
    private val router: SigninContract.Router,private val accountUsecase: AccountUsecase
) : BaseViewModelImpl(),SigninContract.ViewModel {

    override var userNumber: String? = null
    override var userPassword: String? = null

    private var loginDisposable: Disposable? = null

    override fun logginClick() {
        if (TextUtils.isEmpty(userNumber) || TextUtils.isEmpty(userPassword)) {
            Toast.makeText(this, "Введите данные", Toast.LENGTH_SHORT).show()
            return
        }

        loginDisposable?.dispose()


        loginDisposable = accountUsecase.login(userNumber!!, userPassword!!)
            .doOnSubscribe {
                disposables.add(it)
            }.doFinally { }
            .subscribe(this::checkPrivacyPolicy, this::showError)


        //  router.openMainScreen()
    }

    private fun checkPrivacyPolicy() {

    }

    private fun showError(throwable: Throwable) {
        when (throwable) {
            is UnknownHostException -> showError(R.string.network_error)
            else -> showError(R.string.unknown_error)
        }
    }

    override fun bound() {

    }

    override fun onUploadAvatarClick() {
//        if (pickAvatarDialog != null && pickAvatarDialog.isShowing()) {
//            pickAvatarDialog.dismiss()
//            return
//        }
//        pickAvatarDialog = AlertDialog.Builder(Objects.requireNonNull(getContext()))
//            .setItems(R.array.dialog_create_recipe_image_items, { d, which ->
//                onPickAvatarDialogItemSelected(which)
//                d.dismiss()
//            }).setNegativeButton(R.string.cancel, { d, which -> d.dismiss() })
//            .show()
//
//    }
//
//   fun onPickAvatarDialogItemSelected(which) {

    }


    override fun dateClick() {
        router.openDate()

    }


}