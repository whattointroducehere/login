package com.example.login.presentation.base

interface BaseViewModel {
    fun bound()

    fun unbound()
}