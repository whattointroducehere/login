package com.example.login.presentation

import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.example.login.R
import com.example.login.presentation.base.BaseActivity
import org.kodein.di.Kodein.Module
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.singleton

class MainActivity : BaseActivity() {
    override fun diModule() = Module("module name") {
        bind() from singleton { MainRouter(this@MainActivity) }
    }
    private val router: MainRouter by instance()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    companion object {
        fun newIntent(context: Context) = Intent(context, MainActivity::class.java)
    }

}