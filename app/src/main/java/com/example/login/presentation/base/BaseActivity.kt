package com.example.login.presentation.base

import android.content.Intent
import android.os.Bundle
import androidx.annotation.Nullable
import androidx.appcompat.app.AppCompatActivity
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.KodeinTrigger
import org.kodein.di.android.closestKodein
import org.kodein.di.android.retainedKodein

abstract class BaseActivity : AppCompatActivity(), KodeinAware {
    private val _parentKodein by closestKodein()
    override val kodein: Kodein by retainedKodein {
        extend(_parentKodein)
        import(diModule())
    }
    override val kodeinTrigger = KodeinTrigger()

    override fun onCreate(@Nullable savedInstanceState: Bundle?) {
        kodeinTrigger.trigger()
        super.onCreate(savedInstanceState)
    }

    abstract fun diModule(): Kodein.Module



    open fun updateTitleRes(title: Int){}
    open fun showDrawer(){}

    protected fun transactionActivity(activity: Class<*>?, cycleFinish: Boolean) {
        if (activity != null) {
            val intent = Intent(this, activity)
            intent.removeExtra("cmd")
            startActivity(intent)
            if (cycleFinish) {
                this.finish()
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
    }



}