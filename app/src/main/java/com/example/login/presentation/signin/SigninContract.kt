package com.example.login.presentation.signin

import com.example.login.presentation.base.BaseViewModel

interface SigninContract {

    interface ViewModel : BaseViewModel {

        fun logginClick()

        fun dateClick()

        fun onUploadAvatarClick()

        var userNumber: String?
        var userPassword: String?


    }


    interface Router{

        fun openMainScreen()

        fun openDate()

    }
}