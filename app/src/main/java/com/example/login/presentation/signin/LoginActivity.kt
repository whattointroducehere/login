package com.example.login.presentation.signin

import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.example.login.R
import com.example.login.presentation.base.BaseActivity
import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.singleton


class LoginActivity : BaseActivity() {
    override fun diModule() = Kodein.Module("LoginActivity") {
        bind() from singleton { LoginRouter(this@LoginActivity) }
    }

    private val router: LoginRouter by instance()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        router.navigateToSignInFragment()
    }

    companion object {
        fun newIntent(context: Context): Intent {
            val intent = Intent(context, LoginActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
            return intent
        }
    }
//    companion object {
//        @JvmStatic
//        fun newIntent(context: Context) = Intent(context, LoginActivity::class.java)
//    }
}

//class LoginActivity : BaseActivity() {
//
//
//    override fun diModule() = Kodein.Module("Login module") {
//        bind() from provider { LoginRouter(this@LoginActivity) }
//        bind<LoginContract.ViewModel>() with singleton { LoginViewModel(instance() }
//    }
//
//    private val router: LoginRouter by instance()
//    private val viewModel: LoginContract.ViewModel by instance()
//    private var editBirthday: TextView? = null
//    private lateinit var binding: ActivityLoginBinding
//    private var dateSetListener: DatePickerDialog.OnDateSetListener? = null
//
//
//    override fun onCreate(savedInstanceState: Bundle?) {
//        super.onCreate(savedInstanceState)
//        binding=DataBindingUtil.setContentView(this,R.layout.activity_login)
//        setContentView(R.layout.activity_login)
//        binding.viewModel = this@LoginActivity.viewModel
//        viewModel.bound()
//    }
//
//    override fun onCreateView(view: View, savedInstanceState: Bundle?): View? {
//        return super.onCreateView(name, context, attrs)
//    }
//
//
//
//

//
//
//    fun hideKeyboard() {
//        val imm = this.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
//        Objects.requireNonNull(imm).hideSoftInputFromWindow(this.window.decorView.windowToken, 0)
//
//    }
//
//    private fun initListeners(){
//        button.setOnClickListener {viewModel.logginClick() }
//
//    }
//
//
//
//
//
//
//
//}