package com.example.login.presentation.base

interface BaseContract {
    interface View

    interface Presenter<T : View> {
        fun attachView(view: T)

        fun detachView()
    }
}