package com.example.login.presentation.base

import androidx.lifecycle.ViewModel
import io.reactivex.disposables.CompositeDisposable

abstract class BaseViewModelImpl : ViewModel(), BaseViewModel {

    protected val disposables = CompositeDisposable()

    override fun bound() {}
    override fun unbound() {}
}
