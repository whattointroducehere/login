package com.example.login.presentation.splash

import com.example.login.presentation.base.BaseViewModel

interface SplashContract {

    interface ViewModel : BaseViewModel



    interface Router{

        fun goToLoginFragment()

    }

}