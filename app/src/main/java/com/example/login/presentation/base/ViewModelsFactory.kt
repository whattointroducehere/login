package com.example.login.presentation.base

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.login.domain.usecase.AccountUsecase
import com.example.login.presentation.signin.SigninContract
import com.example.login.presentation.signin.SigninViewModel

class SigninFactory(
                    private val accountUsecase: AccountUsecase,
                    private val router: SigninContract.Router) : ViewModelProvider.NewInstanceFactory() {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T =
        SigninViewModel( accountUsecase, router) as T
}