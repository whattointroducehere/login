package com.example.login.presentation.signin

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.example.login.R
import com.example.login.presentation.base.BaseFragment
import org.kodein.di.Kodein
import org.kodein.di.generic.instance

class SigninFragment : BaseFragment() {
    override val kodeinModule: Kodein.Module = SigninModule.module(this)
    private val viewModel: SigninContract.ViewModel by instance()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_signin, container, false)
    }

    private fun showErrorMessage(message: String) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
    }

    companion object {
        fun newInstance() = SigninFragment()
    }
}