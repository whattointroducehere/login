package com.example.login.presentation.signin

import com.example.login.presentation.base.SigninFactory
import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.singleton

object SigninModule {
    fun module(fragment: SigninFragment) = Kodein.Module("SigInFragment") {
        bind() from singleton { SigninFactory(instance(), instance()) }
        bind<SigninContract.Router>() with singleton { instance<LoginRouter>() }
        bind<SigninContract.ViewModel>() with singleton {
            fragment.vm<SigninViewModel>(instance())
        }
    }

}