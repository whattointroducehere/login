package com.example.login.presentation.signin

import com.example.login.R
import com.example.login.presentation.MainActivity
import com.example.login.utils.extintions.replaceWithoutBackStack


class LoginRouter(private val activity: LoginActivity) :
    SigninContract.Router {
    override fun openMainScreen() {
        activity.startActivity(MainActivity.newIntent(activity))
        activity.finish()
    }

    override fun openDate() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    fun navigateToSignInFragment() {
        activity.supportFragmentManager.replaceWithoutBackStack(R.id.loginContainer, SigninFragment.newInstance())
    }
}

