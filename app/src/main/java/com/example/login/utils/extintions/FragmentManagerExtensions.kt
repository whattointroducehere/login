package com.example.login.utils.extintions

import android.annotation.SuppressLint

@SuppressLint("CheckResult")
fun androidx.fragment.app.FragmentManager.replaceWithoutBackStack(containerId: Int,
                                                                  fragment: androidx.fragment.app.Fragment) {
    beginTransaction()
        .replace(containerId, fragment, fragment::class.java.simpleName)
        .commit()
}

fun androidx.fragment.app.FragmentManager.replaceWithBackStack(containerId: Int,
                                                               fragment: androidx.fragment.app.Fragment) {
    beginTransaction()
        .replace(containerId, fragment, fragment::class.java.simpleName)
        .addToBackStack(fragment::class.java.simpleName)
        .commit()
}
