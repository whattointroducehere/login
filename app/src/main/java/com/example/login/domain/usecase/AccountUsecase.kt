package com.example.login.domain.usecase

import io.reactivex.Completable

interface AccountUsecase {
    fun login(userName: String, password: String): Completable
}