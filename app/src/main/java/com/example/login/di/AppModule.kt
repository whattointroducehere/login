package com.example.login.di

import com.example.login.App
import org.kodein.di.Kodein

object AppModule {

    fun module(application: App) = Kodein.Module("AppModule") {


    }
}